const request = require('request');
const cheerio = require('cheerio');

exports.handler = function (event, context, callback) {
  let domain = 'https://tfgm.com';
  let url = `${domain}/public-transport/tram/stops`;

  let removeTramStopSuffix = station => {
    return station.split(' tram stop')[0];
  };

  request(url, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html);

      let response = {};
      response.stops = {};

      let $stopLinks = $("#content").find(`a[href^="/public-transport/tram/stops/"]`);

      $stopLinks.each(function () {
        let id = $(this).attr('id').split('-tram')[0];
        let name = removeTramStopSuffix($(this).text().trim());

        response.stops[id] = {
          id,
          name,
        }
      });

      callback(null, {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': `max-age=0`,
          'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(response)
      });
    } else {
      callback(null, {
        statusCode: 404,
        body: JSON.stringify({
          msg: 'Something went wrong'
        })
      });
    }
  });
};
