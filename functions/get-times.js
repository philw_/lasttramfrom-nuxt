const request = require('request');
const cheerio = require('cheerio');
const parse = require('date-fns/parse');
const getYear = require('date-fns/get_year');
const setHours = require('date-fns/set_hours');
const setMinutes = require('date-fns/set_minutes');
const addDays = require('date-fns/add_days');
const format = require('date-fns/format');

exports.handler = function(event, context, callback) {
  let from_slug = event.queryStringParameters.from;
  let to_slug = event.queryStringParameters.to;

  let url = `https://tfgm.com/public-transport/tram/stops/${from_slug}-tram/tram-times/${to_slug}-tram`;

  let removeTramStopSuffix = station => {
    return station.split(' tram stop')[0];
  };

  request(url, function(error, response, html) {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html);

      let $lastData = $('#first-last-items');

      let times = {};

      let from_proper = removeTramStopSuffix(
        $('h1')
          .text()
          .trim()
      );
      let to_proper = removeTramStopSuffix(
        $('#tram-times-destination-value')
          .text()
          .trim()
      );

      $lastData.find('.row').each(function() {
        {
          let day = $(this)
            .find('.first-last-day')
            .text()
            .trim();

          let dayAsDate = parse(`${day} ${getYear(new Date())}`);
          dayAsDate = format(dayAsDate, 'YYYY-MM-DD');

          $(this).find('#first-last-0 .first-last-last');

          let duration = $('.estimated-time-time')
            .first()
            .text()
            .trim();

          let time = $(this)
            .find('.first-last-last .first-last-departure-time')
            .text()
            .trim();

          let via = $(this)
            .find('.first-last-last .first-last-route')
            .text()
            .trim();

          let timeParts = time.split(':');

          let dayAsDateWithHours = setHours(dayAsDate, timeParts[0]);
          let timeAsDate = setMinutes(dayAsDateWithHours, timeParts[1]);

          if (Number(timeParts[0]) < 4) {
            timeAsDate = addDays(timeAsDate, 1);
          }

          times[dayAsDate] = {
            date: {
              day: {
                ofWeek: format(dayAsDate, 'dddd'),
                ofMonth: format(dayAsDate, 'Do')
              },
              month: {
                short: format(dayAsDate, 'MMM'),
                long: format(dayAsDate, 'MMMM')
              }
            },
            time: format(timeAsDate, 'YYYY-MM-DDTHH:mm:ss'),
            duration
          };

          if (via !== 'Direct') {
            times[dayAsDate].via = via;
          }
        }
      });

      let { latitude, longitude } = JSON.parse($('#microdata').html()).geo;

      let from = {
        slug: from_slug,
        name: from_proper,
        latitude,
        longitude
      };

      let to = {
        slug: to_slug,
        name: to_proper
      };

      let data = {
        to,
        from,
        times,
        url
      };

      callback(null, {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': `max-age=0`,
          'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(data)
      });
    } else {
      callback(null, {
        statusCode: 404,
        body: JSON.stringify({
          msg: 'Something went wrong'
        })
      });
    }
  });
};
