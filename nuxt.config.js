const pkg = require('./package');
const axios = require('axios');
import path from 'path';
import PurgecssPlugin from 'purgecss-webpack-plugin';
import glob from 'glob-all';
const map = require('lodash/map');

class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-z0-9-:/]+/g) || [];
  }
}

const routes = () => {
  return axios
    .get('https://last-tram-from-nuxt.netlify.com/.netlify/functions/get-stops')
    .then(function(response) {
      const stopList = map(
        response.data.stops,
        stop => stop.id.split('-tram')[0]
      );
      const routes = [];

      stopList.map(stop => {
        let stop1 = stop;
        routes.push(stop1);

        stopList.map(stop2 => {
          if (stop1 !== stop2) {
            routes.push(stop1 + '/' + stop2);
          }
        });
      });

      return routes;
    })
    .catch(function(error) {
      console.log(error);
    });
};

module.exports = {
  mode: 'universal',

  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ],
    script: [
      {
        src:
          '//cdn.jsdelivr.net/gh/philwolstenholme/outline.js@master/outline.min.js',
        type: 'text/javascript',
        body: true,
        defer: true,
        crossorigin: undefined
      }
    ],
    htmlAttrs: {
      lang: 'en-gb'
    }
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#ffd700',
    height: '3px',
    continuous: true
  },

  /*
   ** Global CSS
   */
  css: ['~/assets/css/tailwind.css'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/sitemap',
    '@nuxtjs/google-analytics',
    '@nuxtjs/pwa'
  ],

  /*
   ** Axios module configuration
   */
  axios: {
    baseURL: process.env.baseUrl,
    browserBaseURL: process.env.baseUrl
  },

  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    optimizeCSS: true,
    postcss: {
      plugins: {
        tailwindcss: path.resolve('./tailwind.js')
      },
      preset: { autoprefixer: { grid: true } }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }

      if (!ctx.isDev) {
        config.plugins.push(
          new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, './pages/**/*.vue'),
              path.join(__dirname, './layouts/**/*.vue'),
              path.join(__dirname, './components/**/*.vue')
            ]),
            extractors: [
              {
                extractor: TailwindExtractor,
                extensions: ['vue']
              }
            ],
            whitelist: ['html', 'body', 'nuxt-progress']
          })
        );
      }
    },

    filenames: {
      app: ({ isDev }) => (isDev ? '[name].js' : '[name].js'),
      chunk: ({ isDev }) => (isDev ? '[name].js' : '[name].js'),
      css: ({ isDev }) => (isDev ? '[name].css' : '[name].css'),
      img: ({ isDev }) => (isDev ? '[path][name].[ext]' : 'img/[name].[ext]'),
      font: ({ isDev }) =>
        isDev ? '[path][name].[ext]' : 'fonts/[name].[ext]',
      video: ({ isDev }) =>
        isDev ? '[path][name].[ext]' : 'videos/[name].[ext]'
    }
  },

  proxy: {
    '/.netlify/functions': {
      target: 'http://localhost:9000',
      pathRewrite: {
        '^/.netlify/functions': ''
      }
    }
  },

  generate: {
    routes
  },

  sitemap: {
    generate: true,
    path: '/sitemap.xml',
    hostname: 'https://lasttramfrom.com',
    routes
  },

  'google-analytics': {
    id: 'UA-91156304-1'
  },

  workbox: {
    globPatterns: ['**/*.{js,css,htm,html,png,gif,jpeg}', '*.{js,css,htm,html}']
  }
};
